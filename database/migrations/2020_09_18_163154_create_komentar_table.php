<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('isikomen');
            $table->unsignedBigInteger('profile_id');
            $table->foreign('profile_id')->references('id')->on('profile'); 
            $table->unsignedBigInteger('postingan_id');
            $table->foreign('postingan_id')->references('id')->on('postingan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar');
        $table->dropForeign('postingan_id');
        $table->dropColumn('postingan_id');
        $table->dropForeign('profile_id');
        $table->dropColumn('profile_id');
        $table->dropColumn('isikomen');
        $table->dropColumn('id');
    }
}
