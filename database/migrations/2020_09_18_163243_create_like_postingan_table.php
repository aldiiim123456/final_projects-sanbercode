<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikePostinganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_postingan', function (Blueprint $table) {
            $table->unsignedBigInteger('profile_id');
            $table->unsignedBigInteger('postingan_id');
            $table->unsignedBigInteger('poin');
            $table->foreign('profile_id')->references('id')->on('profile');
            $table->foreign('postingan_id')->references('id')->on('postingan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_postingan');
        $table->dropForeign('postingan_id');
        $table->dropForeign('profile_id');
        $table->dropColumn('poin');
        $table->dropColumn('postingan_id');
        $table->dropColumn('profile_id');

    }
}
