<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;

class FinalController extends Controller
{
    public function login(){
        return view('login');
    }

    public function register(){
        return view('register');
    }

    public function create() {
    	return view('create');
    }

    public function store(Request $request) {
        //dd($request->all());
        $request->validate([
            'foto' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'caption'=>'required'
        ]);
        $querry = DB::table('postingan')->insert([
            "foto" => $request["foto"],
            "caption" => $request["caption"]
        ]);
       

            /*$index = Post::create([
                "caption" => $request["caption"],
                "foto" => $request["foto"]
            ]); */

        return redirect('/profile') -> with ('success','Postingan Berhasil dibuat');
    }

    public function index(){
        $index = DB::table('postingan')->get();
        //dd($index);
        return view('showprofile');
    }
    
    public function show($id){
        $show = DB::table('postingan')->where('id', $id)->first();
        return view('showpostingan', compact('show'));
    }

    public function edit($id){
        $edit = DB::table('postingan')->where('id',$id)->first();
        return view('edit', compact('edit'));
    }

    public function update($id, Request $request){
        $request->validate([
            'caption'=>'required'
        ]);

        $querry = DB::table('postingan')
                        ->where('id', $id)
                        ->update([
                            'caption'=>$request['caption']
                        ]);

        return redirect('/profile')->with('success', 'Berhasil Mengedit pertanyaan!');
    }

    public function destroy($id){
        $querry = DB::table('postingan')->where('id', $id)->delete();
        return redirect('/profile')->with('success','Berhasil Menghapus Pertanyaan');
    }

    public function home(){
        return view('indexpostingan');
    }

}
