@extends('master.master')

@section('content')
					<div class="card container" style="opacity: 80%;">
					<div class="card-body">
					<div class="post">
                      <p>
                        <img src="{{ $show->foto }}">
                        <span> 
                          {{ $show -> caption }}
                        </span>
                      </p>

                      <p>
                        <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                        <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                        <span class="float-right">
                          <a href="#" class="link-black text-sm">
                            <i class="far fa-comments mr-1"></i> Comments (5)
                          </a>
                        </span>
                      </p>

                      <input class="form-control form-control-sm mb-2" type="text" placeholder="Type a comment">
                     </div>
                     </div>
                     <div class="post clear-fix ml-3 mr-3">
                      <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="../../dist/img/user7-128x128.jpg" alt="User Image">
                        <span class="username">
                          <a href="#" style="color:#ff105f">Sarah Ross</a>
                          <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                        </span>
                        <span class="description">Sent you a message - 3 days ago</span>
                      </div>
                      <!-- /.user-block -->
                      <p>
                        Lorem ipsum represents a long-held tradition for designers,
                        typographers and the like. Some people hate it and argue for
                        its demise, but others ignore the hate as they create awesome
                        tools to help create filler text for everyone from bacon lovers
                        to Charlie Sheen fans.
                      </p>

                      <form class="form-horizontal" style="padding-bottom: 20px;">
                        <div class="input-group input-group-sm mb-0">
                          <input class="form-control form-control-sm" placeholder="Response">
                          <div class="input-group-append">
                            <button type="submit" class="btn btn-danger" style="background: linear-gradient(to right,#ff105f,#ffad06)">Send</button>
                          </div>
                        </div>
                      </form>
                    </div>
                     </div>
@endsection