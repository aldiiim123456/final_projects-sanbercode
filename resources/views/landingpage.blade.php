<!DOCTYPE html>
<html>
<head>
	<title>Hello World</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/landingpage/style.css') }}">
	  <link rel="stylesheet" href="{{ asset('/dist/css/adminlte.min.css') }}">
</head>
<body>
	<div class="bg">
		<img src="{{ asset('/landingpage/logo.png') }}" style="width: 300px;">
		<br>
		<label>WELCOME TO HELLO WORLD.COM</label>
		<p>See what’s happening in the world right now</p>
		<label class="lbl">Join Hello World today.</label>
		<br>

		<a href="/register"><button class="btn btn-lg btn-primary" style="margin-left: 40px;">Getting Started</button></a>


	</div>
</div>
</body>
</html>