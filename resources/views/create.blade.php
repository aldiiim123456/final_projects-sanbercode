@extends('master.master')
@section('content')
  <div class="ml-2 mt-5 ">
  <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Create Post</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="/profile" method="POST">
              	@csrf
                <!-- text input -->
                <div class="form-group">
                  <label for="foto">Foto</label>
                  <div class="input-group">
              <div class="custom-file">
              <input type="file" class="custom-file-input" id="foto" aria-describedby="inputGroupFileAddon04" name="foto" value="foto" required>
              <label class="custom-file-label" for="foto">Choose file</label>
              </div>
              <div class="input-group-append">
              </div>
              </div>
                <div class="form-group">
                  <label for="judul">Caption</label>
                  <textarea name="caption" value="{{ old('caption','') }}" class="form-control" placeholder="Enter ..."></textarea>
                  @error('caption')
    				<div class="alert alert-danger">{{ $message }}</div>
				          @enderror
                  <button type="submit" class="btn btn-primary" style="background: linear-gradient(to right,#ff105f,#ffad06">Posting!</button>

              </form>
            </div>
            <!-- /.box-body -->
    </div>
</div>
@endsection