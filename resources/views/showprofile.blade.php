  @extends('master.master')

@section('content')

      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6 md-9 lg-12">
            <h1>Profile</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->

		<div class="row mt-5">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">Nina Mcintire</h3>

                <p class="text-muted text-center">Software Engineer</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li>
                </ul>

                <a href="#" class="btn btn-block" style="background: linear-gradient(to right,#ff105f,#ffad06)";><b>Follow</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i> Education</strong>

                <p class="text-muted">
                  B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                <p class="text-muted">Malibu, California</p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Skills</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">UI Design</span>
                  <span class="tag tag-success">Coding</span>
                  <span class="tag tag-info">Javascript</span>
                  <span class="tag tag-warning">PHP</span>
                  <span class="tag tag-primary">Node.js</span>
                </p>

                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>

                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
            <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Timeline</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Update Profile</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <!-- /.tab-pane -->
                  <div class=" active tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">
                      <!-- timeline time label -->
                      <div class="time-label">
                        <span class="bg-danger">
                          10 Feb. 2014
                        </span>
                      </div>
                      <!-- /.timeline-label -->
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-envelope bg-primary"></i>
                       
                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 12:05</span>

                          <h3 class="timeline-header"><a href="#">Your Post</a></h3>
                          <div class="timeline-body">
                          <p>Seharus nya disini menampilkan postingan</p>
                          </div>
                          <div class="timeline-footer" style="display: flex;">
                            <a href="/profile" class="btn btn-primary btn-sm">Read more</a>
                            <a href="/profile/edit" class="btn btn-success btn-sm ml-2">Edit</a>
                            <form action="/profile" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" name="delete"class="btn btn-danger btn-sm ml-2" value="delete"> 
                          </form>
                          </div>
                        </div>
                      
                      </div>
                    </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal">
                      <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="name" placeholder="Nama Lengkap" name="name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="bio" class="col-sm-2 col-form-label">Biografi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="bio" value="bio" name="bio"></textarea> 
                        </div>
                      </div>
                    <div class="form-group row">  
                  <label for="tgllahir" class="col-sm-2 col-form-label">Date masks:</label>
                  <div class="col-sm-10">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask="" im-insert="false" name="tgllahir">
                  </div>
              </div>
              </div>
                  <!-- /.input group -->
                
                      <div class="form-group row">
                       <label for="foto" class="col-sm-2 col-form-label">Foto</label>
                       <div class="col-sm-10">
                      <div class="input-group">
  						<div class="custom-file">
    					<input type="file" class="custom-file-input" id="foto" aria-describedby="inputGroupFileAddon04" name="foto" value="foto">
    					<label class="custom-file-label" for="foto">Choose file</label>
  						</div>
  						<div class="input-group-append">
    					<button class="btn btn-outline-primary" type="button" id="inputGroupFileAddon04">Upload</button>
  						</div>
  						</div>
						</div>
					</div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn" style="background: linear-gradient(to right,#ff105f,#ffad06">Update Profile</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
    
@endsection