<!DOCTYPE html>
<html>
<head>
	<title>Registration form</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/loginregister/register.css') }}">
</head>
<body>
	<div class="login">
		<div class="form-box">
			<div class="button-box">
				<div id="btn"></div>
				<button type="button" class="toggle-btn">Register</button>
				<button type="button" class="toggle-btn"><a href="/">Login</a></button>					
			</div>
			<form id="register" class="input-group" action="/">
				<input type="Email" class="input-field" placeholder="Email Id" name="email" required>
				<input type="text" class="input-field"  placeholder="User Name" name="name" required>
				<input type="Password" class="input-field" placeholder="Enter Password" name="password" required>
				<input type="checkbox" class="check-box" required><span>I agree with term & condition</span>
				<button type="submit" class="submit-btn">Register</button>
			</form>
		</div>
	</div>
	</body>
</html>