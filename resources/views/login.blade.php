<!DOCTYPE html>
<html>
<head>
	<title>Login form</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/loginregister/login.css') }}">
</head>
<body>
	<div class="login">
		<div class="form-box">
			<img src="{{ asset('/loginregister/avatar.png') }}" class="avatar">
			<div class="button-box">
				<div id="btn"></div>
				<button type="button" class="toggle-btn">Log In</button>
				<button type="button" class="toggle-btn"><a href="/register">Register</a></button>					
			</div>
			<form  action="/home" id="login" class="input-group">
				<input type="text" class="input-field" placeholder="User Id" required>
				<input type="Password" class="input-field" placeholder="Enter Password" required>
				<input type="checkbox" class="check-box"><span>Remember Password</span>
				<button type="submit" class="submit-btn">Log In</button>
			</form>
		</div>
	</div>
</body>
</html>