<nav class="navbar navbar-expand navbar-dark navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/home" style="color:#ff105f;font-family:Arial Black, Gadget, sans-serif;font-size: large; " class="nav-link"> Hello World  </a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/home" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/profile" class="nav-link">Myprofile</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline" style="margin-left: 200px;">
      <div class="input-group input-group-md">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" style="width: 500px;">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
    <ul>
      <li class="nav-item d-none d-sm-inline-block ml-3 mt-2">        
        <a href="/create" class="btn btn-sm" style="background: linear-gradient(to right,#ff105f,#ffad06);">Create Post</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block ml-3 mt-2">
        <a href="/" class="btn btn-sm" style="background: linear-gradient(to right,#ff105f,#ffad06);">Log Out</a>
      </li>
    </ul>
  </nav>