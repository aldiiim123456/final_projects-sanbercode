@extends('master.master')

@section('content')
					<div class="card container" style="opacity: 80%;">
					<div class="card-body">
           
					<div class="post">
                      <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
                        <span class="username">
                          <a href="#" style="color:#ff105f">Jonathan Burke Jr.</a>
                          <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                        </span>
                        <span class="description">Shared publicly - 7:30 PM today</span>
                      </div>
                      <!-- /.user-block -->
                      <img src="{{ asset('/loginregister/walpaper.jpg') }}" style="height: 120px;">
                      <p>
                        Pemandangan di Luar negeri 29-05-2020
                      </p>

                      <p>
                        <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                        <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                        <span class="float-right">
                          <a href="#" class="link-black text-sm">
                            <i class="far fa-comments mr-1"></i> Comments
                          </a>
                        </span>
                      </p>

                      <input class="form-control form-control-sm mb-2" type="text" placeholder="Type a comment">
                     </div>
                     </div>
                     </div>
@endsection