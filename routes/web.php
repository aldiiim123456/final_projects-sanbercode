<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FinalController@login');
Route::get('/register','FinalController@register');
Route::get('/create','FinalController@create');
Route::post('/profile','FinalController@store');
Route::get('/profile','FinalController@index');
Route::get('profile/{id}','FinalController@show');
Route::get('/profile/{id}/edit','FinalController@edit');
Route::put('profile/{id}','FinalController@update');
Route::delete('profile/{id}','FinalController@destroy');
Route::get('/home','FinalController@home');
Route::get('/homepage', function(){
	return view('landingpage');
});



